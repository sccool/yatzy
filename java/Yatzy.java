import java.util.ArrayList;
import java.util.List;

public class Yatzy {

	public static int chance(int... dices) {
		return sumOfAllValues(dices);
	}

	public static int yatzy(int... dices) {
		return _distinctValues(dices).length == 1 ? 50 : 0;
	}

	public static int ones(int... dice) {
		return _sumAllValuesOf(1, dice);
	}

	public static int twos(int... dice) {
		return _sumAllValuesOf(2, dice);
	}

	public static int threes(int... dice) {
		return _sumAllValuesOf(3, dice);
	}

	public static int fours(int... dice) {
		return _sumAllValuesOf(4, dice);
	}

	public static int five(int... dice) {
		return _sumAllValuesOf(5, dice);
	}

	public static int sixies(int... dice) {
		return _sumAllValuesOf(6, dice);
	}

	public static int score_pair(int... dices) {
		int maxSumPair = 0;
		for (Integer value : _distinctValues(dices)) {
			int sumPairScore = _sumNbOccurenceOfValue(value, 2, dices);
			if (sumPairScore > maxSumPair) {
				maxSumPair = sumPairScore;
			}
		}
		return maxSumPair;
	}

	public static int two_pair(int... dices) {
		int totalPair = 0;
		for (Integer value : _distinctValues(dices)) {
			totalPair += _sumNbOccurenceOfValue(value, 2, dices);
		}
		return totalPair;
	}

	public static int three_of_a_kind(int... dices) {
		return sumOfKind(3, dices);
	}

	public static int four_of_a_kind(int... dices) {
		return sumOfKind(4, dices);
	}

	public static int fullHouse(int... dices) {
		return _distinctValues(dices).length == 2 ? sumOfAllValues(dices) : 0;
	}

	public static int smallStraight(int... dices) {
		return straight(dices);
	}

	public static int largeStraight(int... dices) {
		return straight(dices);
	}
	
	private static int straight(int... dices) {
		return _distinctValues(dices).length == 5 ? sumOfAllValues(dices) : 0;
	}

	private static int sumOfAllValues(int... dices) {
		return _sumAllValuesOf(null, dices);
	}

	private static int _sumAllValuesOf(Integer valueToSum, int... dice) {
		int total = 0;
		for (int die : dice) {
			if (valueToSum == null || valueToSum == die) {
				total += die;
			}
		}
		return total;
	}

	private static Integer[] _distinctValues(int... dices) {
		List<Integer> list = new ArrayList<Integer>();
		for (Integer die : dices) {
			if (!list.contains(die)) {
				list.add(die);
			}
		}
		return list.toArray(new Integer[list.size()]);
	}

	private static int sumOfKind(int nbOfKind, int... dices) {
		for (Integer value : _distinctValues(dices)) {
			int sum = _sumNbOccurenceOfValue(value, nbOfKind, dices);
			if (sum > 0) {
				return sum;
			}
		}
		return 0;
	}

	public static int _sumNbOccurenceOfValue(int value, int nbOccurence, int... dices) {
		if ((_sumAllValuesOf(value, dices) / value) >= nbOccurence) {
			return value * nbOccurence;
		}
		return 0;
	}

}
